from distutils.core import setup
setup(
  name = 'HTTPrequest',
  packages = ['HTTPrequest'],
  version = '0.1', 
  license='MIT',
  description = 'Command line http client', 
  author = 'BM835',
  author_email = 'support@bm835.space', 
  url = 'https://gitlab.com/BM835/httprequest',   
  download_url = 'https://gitlab.com/BM835/httprequest/-/archive/v0.1/httprequest-v0.1.tar.gz',
  keywords = ['http', 'client', 'command line'],
  install_requires=[],
  classifiers=[
    'Development Status :: 3 - Alpha',
    'Intended Audience :: Developers',
    'Topic :: Software Development :: Build Tools',
    'License :: OSI Approved :: MIT License',
    'Programming Language :: Python :: 3',
    'Programming Language :: Python :: 3.4',
    'Programming Language :: Python :: 3.5',
    'Programming Language :: Python :: 3.6',
  ],
)