# HTTPrequest

![https://gitlab.com/BM835/HTTPrequest/pipelines](https://gitlab.com/BM835/HTTPrequest/badges/master/build.svg) ![GitHub repo size](https://img.shields.io/github/repo-size/bm835/HTTPrequest) ![PyPI](https://img.shields.io/pypi/v/HTTPrequest)

HTTPrequest is python library

## usage
```
$ python -m httprequest domain port
```