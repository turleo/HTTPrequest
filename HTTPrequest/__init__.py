import socket
import sys

host = ''
port = 80
path = '/'
try:
	if sys.argv[1] == "--help":
		print('Usage: \n\t httpreqest domain port path\n\t\t default port = 80 and path = /')
		sys.exit(0)
	host = sys.argv[1]
	port = int(sys.argv[2])
	path = sys.argv[3]
except IndexError as e:
	if host != '':
		pass
	else:
		print('Usage: \n\t httpreqest domain port path\n\t\t default port = 80 and path = /')
		sys.exit(0)
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((host, port))
client.sendall(b'GET ' + path.encode('utf-8') + b' HTTP/1.1\r\nHost: ' + host.encode('utf-8') + b'\r\n\r\n')
    
try:
    data = client.recv(1024)
    print(data.decode('utf-8'))
except:
    print('Oh no %s' % sys.exc_info()[0])
client.close()
sys.exit(0)
